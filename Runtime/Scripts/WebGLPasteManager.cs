using UnityEngine;
#if UNITY_WEBGL && !UNITY_EDITOR
using UnityEngine.EventSystems;
using UnityEngine.UI;
#endif

namespace Walid.System.WebGL
{
    using global::System.Runtime.InteropServices;
    public static class WebGLRuntime
    {
        [DllImport("__Internal")]
        public static extern void PasteInit(string WebGLGameObjectNamePTR, string WebGLMethodPTR);
    }

    public class WebGLPasteManager : MonoBehaviour
    {
        #region Singleton

        private static WebGLPasteManager _instance;

        public static WebGLPasteManager Singleton
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<WebGLPasteManager>();
                    if (_instance == null)
                    {
                        GameObject webGLPasteManager = new GameObject();
                        webGLPasteManager.name = typeof(WebGLPasteManager).Name;
                        _instance = webGLPasteManager.AddComponent<WebGLPasteManager>();
                        DontDestroyOnLoad(webGLPasteManager);
                    }
                }
                return _instance;
            }
        }

        public static void SetNewSingleTon(WebGLPasteManager webGLPasteManager)
        {
            if (_instance != null)
            {
                return;
            }

            _instance = webGLPasteManager;
        }
        #endregion Singleton

        public void Init()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            WebGLRuntime.PasteInit(Singleton.gameObject.name, string.Format("Paste"));
#endif
        }

#if UNITY_WEBGL && !UNITY_EDITOR
        /// <summary>
        /// Called when the user pastes the given value in the Web-Browser.
        /// </summary>
        /// <param name="value">The pasted value.</param>
        public void Paste(string value)
        {
            if (string.IsNullOrEmpty(value)) return;
            else
            {
                if (string.IsNullOrEmpty(GUIUtility.systemCopyBuffer))
                {
                    GameObject currentSelectedObj = EventSystem.current.currentSelectedGameObject;
                    if (currentSelectedObj is not null)
                    {
                        InputField inputField = currentSelectedObj.GetComponentInChildren<InputField>();
                        if (inputField is not null)
                        {
                            string text = string.Empty;
                            if (inputField.selectionFocusPosition >= inputField.selectionAnchorPosition)
                                text = string.Format("{0}{1}{2}", inputField.text.Substring(0, inputField.selectionAnchorPosition), value, inputField.text.Substring(inputField.selectionFocusPosition));
                            else
                                text = string.Format("{0}{1}{2}", inputField.text.Substring(0, inputField.selectionFocusPosition), value, inputField.text.Substring(inputField.selectionAnchorPosition));
                            inputField.text = text;
                            inputField.caretPosition += GUIUtility.systemCopyBuffer.Length;
                        }
                    }
                }
                GUIUtility.systemCopyBuffer = value;
            }
        }
#endif
    }
}