using UnityEngine;
using Walid.System.WebGL;

namespace Walid.Test
{

    public class Test : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            WebGLPasteManager.Singleton.Init();
        }
    }
}