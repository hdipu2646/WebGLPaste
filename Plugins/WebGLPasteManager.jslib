mergeInto(LibraryManager.library, {
    PasteInit: function(WebGLGameObjectNamePTR, WebGLMethodPTR) {
        var WebGLGameObjectName = UTF8ToString(WebGLGameObjectNamePTR);
        var WebGLMethod = UTF8ToString(WebGLMethodPTR);
        document.body.addEventListener("paste", function(e) {
            e.preventDefault();
            var text = (e.originalEvent || e).clipboardData.getData('text/plain');
            SendMessage(WebGLGameObjectName, WebGLMethod, text);
        });
    }
});
